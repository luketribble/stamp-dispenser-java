This program takes in a denomination of stamps, and calculates the minimum number of stamps
needed using those denominations to reach a desired inputted total. Simply edit and run main to execute the program.

This code serves as a solution to the Knapsack problem.