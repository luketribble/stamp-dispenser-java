/*
 * Name:Luke Tribble
 * File: StampDispenser.Java
 * Date: 10-20-17
 * Description: This program takes in a denomination
 * of stamps, and calculates the minimum number of stamps
 * needed using those denominations to reach a desired inputted
 * total.
 * /

/**
 * Facilitates dispensing stamps for a postage stamp machine.
 */
public class StampDispenser
{
  private int[] denominationsmember; //Copies stamp array over for access.
  private boolean onefound; //Flag to detect if a 1 is in the denominations.
    
    /**
     * Constructs a new StampDispenser that will be able to dispense the given 
     * types of stamps.
     *
     * @param stampDenominations The values of the types of stamps that the 
     *     machine should have.  Should be sorted in descending order and 
     *     contain at least a 1.
     */
    public StampDispenser(int[] stampDenominations)
    {
      onefound = false;
      //First, check to see if an empty array is inputted.
      if(stampDenominations.length == 0)
      {
        System.out.println("Error: Inputted an empty array.");
        System.exit(0);
      }
      
      //Next, check to see if 1 is a valid denomination.
      for(int i = 0; i < stampDenominations.length; i++)
      {
        if( stampDenominations[i] == 1)
          
        {
         onefound = true;
        }
      }
      if(onefound == false)
      {
        System.out.println("You must input an array containing at least 1.");
        System.exit(0);
      }
      //Next, we ensure the array is in descending order as specified.
      for(int i = 0; i < stampDenominations.length - 1; i++)
      {
        if(stampDenominations[i+1] > stampDenominations[i])
        {
          System.out.println("Array is not in descending order.");
          System.exit(0);
        }
      }
      //Lastly, we check to make sure no denominations lower than 1 are present.
      for(int i = 0; i < stampDenominations.length; i++)
      {
        if(stampDenominations[i] < 1)
        {
          System.out.println("The lowest denomination a stamp may be is 1.");
          System.exit(0);
        }
      }
      //Copy over the argument array to a new one for accessibility.
      denominationsmember = new int[stampDenominations.length];
      System.arraycopy(stampDenominations,0,denominationsmember,0,stampDenominations.length);
    }
 
    /**
     * Returns the minimum number of stamps that the machine can dispense to
     * fill the given request.
     *
     * @param request The total value of the stamps to be dispensed.
     */
    public int calcMinNumStampsToFillRequest(int request)
    { //Now that base cases are taken care of, lets compute the # of stamps!
      //Minrequest is an array to track the # of stamps needed from 0 to the input.
      int[] minrequest = new int[1+request];
      int[] mindenominations = new int[denominationsmember.length];
      //Mindenominations is an array to track # of stamps using each denom intially.
      //
      minrequest[0] = 0;
      //Loop from 1 stamp denomination to inputted stamp denomination.
      //Go through all denominations to figure out which one to use.
      
      for(int i = 1; i <= request; i++)
      {
        for(int j = 0; j < denominationsmember.length; j++)
        { //Represents each time a denomination is made.
          mindenominations[j] = Integer.MIN_VALUE;
        } //Check if we can use this stamp...
        for(int k = 0; k < denominationsmember.length; k++)
        {
          if(i >= denominationsmember[k]) //This stamp can be used.
          {
            mindenominations[k] = minrequest[i - denominationsmember[k]] + 1;
            //Make the stamp.
          }
        }
        
        minrequest[i] = Integer.MIN_VALUE;
        //Go through and find the minimum stamp amount.
        for(int j = 0; j < denominationsmember.length; j++)
        {
          if(0 <= mindenominations[j])
          { //If it is the minimum...
            if(minrequest[i] == Integer.MIN_VALUE)
            {
              minrequest[i] = mindenominations[j];
            } //Or it is less than the minimum...
            else if(mindenominations[j] < minrequest[i])
            {
              minrequest[i] = mindenominations[j];
            } //Then this is the smallest account.
          }
        }
        
      } //Return the calculation to find the # of stamps made.
      return minrequest[request];
    }
    
    public static void main(String[] args)
    {
        int[] denominations = { 100, 4,2, };
        StampDispenser stampDispenser = new StampDispenser(denominations);
        System.out.println(stampDispenser.calcMinNumStampsToFillRequest(334));
    }
}
